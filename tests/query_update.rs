mod shared;
use shared::*;

#[test]
fn method_update() -> Result<()> {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property", action = "update")]
    pub struct Create {
        pub id: String,
        pub description: String,
        pub age: Option<i64>,
    }

    let p = Create {
        id: "Kalle".into(),
        description: "hejsan svejsan".into(),
        age: None,
    };

    let arr = vec![p];
    let res = Create::update_query(arr)?;
    assert_eq!(
        res.sql(),
        "UPDATE property AS x SET description = x2.description FROM (VALUES ($1, $2)) AS x2 (id, description) WHERE x2.id = x.id RETURNING *;"
    );
    Ok(())
}

#[test]
fn method_update_with_extra_type_support() -> Result<()> {
    #[derive(Debug, sqlx::Type, Clone)]
    #[sqlx(type_name = "cars")]
    pub enum Cars {
        Volvo,
        Saab,
    }
    #[derive(Debug, Crudify)]
    #[crudify(table = "property", action = "update")]
    pub struct Create {
        #[crudify(uuid)]
        pub id: Uuid,
        #[crudify(cars)]
        pub cars: Option<Cars>,
    }

    let p = Create {
        id: Uuid::from_str(UUID_1).unwrap(),
        cars: Some(Cars::Volvo),
    };

    let arr = vec![p];
    let res = Create::update_query(arr)?;
    assert_eq!(
        res.sql(),
        "UPDATE property AS x SET cars = x2.cars::cars FROM (VALUES ($1, $2)) AS x2 (id, cars) WHERE x2.id::uuid = x.id RETURNING *;"
    );
    Ok(())
}

#[test]
fn method_update_with_alt_id() -> Result<()> {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property", action = "update", id = "user_id")]
    pub struct Update {
        #[crudify(uuid)]
        pub user_id: Uuid,
        #[crudify(cars)]
        pub cars: Option<String>,
    }

    let p = Update {
        user_id: Uuid::from_str(UUID_1).unwrap(),
        cars: Some(String::from("volvo")),
    };

    let res = Update::update_query(vec![p])?;
    assert_eq!(
        res.sql(),
        "UPDATE property AS x SET cars = x2.cars::cars FROM (VALUES ($1, $2)) AS x2 (user_id, cars) WHERE x2.user_id::uuid = x.user_id RETURNING *;"
    );
    Ok(())
}

#[test]
fn method_update_error_on_non_uniform_data() -> Result<()> {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property", action = "update")]
    pub struct Create {
        pub id: String,
        pub description: Option<String>,
        pub age: Option<i64>,
    }

    let p1 = Create {
        id: "Kalle".into(),
        description: Some("hejsan svejsan".into()),
        age: None,
    };

    let p2 = Create {
        id: "Kalle".into(),
        description: None,
        age: Some(31),
    };

    match Create::update_query(vec![p1, p2]) {
        Ok(_) => panic!("should error"),
        Err(x) => {
            let text = x.to_string();
            assert_eq!(
                text,
                "error communicating with database: Items are not uniform"
            );
        }
    }

    Ok(())
}
