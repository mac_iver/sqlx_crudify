#![allow(dead_code)]
pub use sqlx::{Postgres, QueryBuilder};
pub use sqlx_crudify::{Crudify, QueryParams};
pub use std::str::FromStr;
pub use uuid::Uuid;

pub type Result<T> = std::result::Result<T, sqlx::Error>;

pub const UUID_1: &'static str = "740ccc2e-3c45-11ed-a261-0242ac120002";
pub const UUID_2: &'static str = "830a5b38-3c45-11ed-a261-0242ac120002";
