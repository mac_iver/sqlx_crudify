mod shared;
use std::collections::BTreeMap;

use shared::*;

#[test]
fn delete_all() -> Result<()> {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property", action = "delete")]
    pub struct Delete;

    let res = Delete::delete_query(None, QueryParams::default())?;
    assert_eq!(res.sql(), "DELETE FROM property * RETURNING id");
    Ok(())
}
#[test]
fn delete_id() -> Result<()> {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property", action = "delete", id = "user_id")]
    pub struct Delete;

    let ids = Some(vec![
        Uuid::from_str(UUID_1).unwrap(),
        Uuid::from_str(UUID_2).unwrap(),
    ]);

    let res = Delete::delete_query(ids, QueryParams::default())?;
    assert_eq!(
        res.sql(),
        "DELETE FROM property * WHERE user_id = ANY($1) RETURNING user_id"
    );
    Ok(())
}

#[test]
fn delete_with_options() -> () {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property", action = "delete")]
    pub struct Delete;

    let params = QueryParams {
        r#where: Some(BTreeMap::from([("surname".into(), "(ew)son".into())])),
        ..Default::default()
    };

    let ids = Some(vec![
        Uuid::from_str(UUID_1).unwrap(),
        Uuid::from_str(UUID_2).unwrap(),
    ]);

    let res = Delete::delete_query(ids, params.start_with_and()).unwrap();

    assert_eq!(
        res.sql(),
        "DELETE FROM property * WHERE id = ANY($1) AND surname '%son' RETURNING id"
    );
}
