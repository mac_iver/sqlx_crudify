mod shared;
use std::collections::BTreeMap;

use shared::*;

#[test]
fn read_all() -> () {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property", action = "read")]
    pub struct Read;

    let res = Read::read_query(None, QueryParams::default()).unwrap();

    assert_eq!(res.sql(), "SELECT * FROM property");
}

#[test]
fn read_ids() -> () {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property", action = "read")]
    pub struct Read;

    let ids = Some(vec![
        Uuid::from_str(UUID_1).unwrap(),
        Uuid::from_str(UUID_2).unwrap(),
    ]);

    let res = Read::read_query(ids, QueryParams::default()).unwrap();

    assert_eq!(res.sql(), "SELECT * FROM property WHERE id = ANY($1)");
}

#[test]
fn read_with_options() -> () {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property", action = "read")]
    pub struct Read;

    let params = QueryParams {
        r#where: Some(BTreeMap::from([("surname".into(), "(ew)son".into())])),
        ..Default::default()
    };

    let ids = Some(vec![
        Uuid::from_str(UUID_1).unwrap(),
        Uuid::from_str(UUID_2).unwrap(),
    ]);

    let res = Read::read_query(ids, params.start_with_and()).unwrap();

    assert_eq!(
        res.sql(),
        "SELECT * FROM property WHERE id = ANY($1) AND surname '%son'"
    );
}
