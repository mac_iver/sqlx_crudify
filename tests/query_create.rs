mod shared;
use shared::*;

#[test]
fn create_query() -> Result<()> {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property", action = "create")]
    pub struct Create {
        pub name: String,
        pub age: Option<i64>,
        pub uuid: Option<Uuid>,
    }

    let p = Create {
        name: "Kalle".into(),
        age: None,
        uuid: Uuid::from_str(UUID_1).ok(),
    };

    let res = Create::create_query(vec![p])?;
    assert_eq!(
        res.sql(),
        "INSERT INTO property (name, uuid) VALUES ($1, $2) RETURNING *"
    );
    Ok(())
}

#[test]
fn create_query_error_on_non_uniform() -> Result<()> {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property", action = "create")]
    pub struct Create {
        pub name: String,
        pub age: Option<i64>,
        pub uuid: Option<Uuid>,
    }

    let p1 = Create {
        name: "Kalle 1".into(),
        age: Some(1),
        uuid: None,
    };

    let p2 = Create {
        name: "Kalle 2".into(),
        age: None,
        uuid: Uuid::from_str(UUID_1).ok(),
    };

    let res = Create::create_query(vec![p1, p2]);

    match res {
        Ok(_) => panic!("should error"),
        Err(x) => {
            let text = x.to_string();
            assert_eq!(
                text,
                "error communicating with database: Items are not uniform"
            );
        }
    }

    Ok(())
}

#[test]
fn create_query_error_on_missing_data() -> Result<()> {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property", action = "create")]
    pub struct Create {
        pub name: String,
    }

    let res = Create::create_query(vec![]);

    match res {
        Ok(_) => panic!("should error"),
        Err(x) => {
            let text = x.to_string();
            assert_eq!(text, "error communicating with database: Missing item");
        }
    }

    Ok(())
}
