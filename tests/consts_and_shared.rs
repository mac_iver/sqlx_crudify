mod shared;
use shared::*;

#[test]
fn get_id() -> Result<()> {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property")]
    pub struct Create {
        pub name: String,
        pub age: Option<i64>,
    }

    assert_eq!(Create::ID, "id");

    Ok(())
}

#[test]
fn get_table_name() -> Result<()> {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property")]
    pub struct Create {
        pub name: String,
        pub age: Option<i64>,
    }

    assert_eq!(Create::TABLE, "property");

    Ok(())
}

#[test]
fn get_fields() -> Result<()> {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property")]
    pub struct Create {
        pub name: String,
        pub age: Option<i64>,
    }

    assert_eq!(Create::FIELDS, &["age", "name"]);

    Ok(())
}

#[test]
fn print_struct_type_schema() -> Result<()> {
    #[derive(Debug, Crudify)]
    #[crudify(table = "property", action = "create")]
    pub struct Create {
        pub name: String,
        pub age: Option<i64>,
        pub uuid: Option<Uuid>,
    }

    let p1 = Create {
        name: "Kalle".into(),
        age: None,
        uuid: Uuid::from_str(UUID_1).ok(),
    };

    let p2 = Create {
        name: "Kalle".into(),
        age: Some(33),
        uuid: None,
    };

    let p3 = Create {
        name: "Sture Sventon".into(),
        age: Some(11),
        uuid: None,
    };

    assert_eq!(&p1.query_schema(), &[2, 0, 1]);
    assert_eq!(&p2.query_schema(), &[2, 1, 0]);
    assert_ne!(p1.query_schema(), p2.query_schema());
    assert_eq!(p2.query_schema(), p3.query_schema());

    Ok(())
}
