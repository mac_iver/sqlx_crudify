mod query_params;
mod traits;

pub use query_params::*;
pub use sqlx::Postgres;
pub use sqlx::QueryBuilder;
pub use sqlx_crudify_derive::*;
pub use traits::*;
pub use uuid::Uuid;

pub static ESCAPED_CHARS: &[char] = &[
    '\'', '"', '\\', ';', '-', '*', '/', '<', '>', '&', '|', '#', '!', '%',
];
