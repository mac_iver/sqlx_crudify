mod condition;
mod order_dir;

use crate::ESCAPED_CHARS;
pub use condition::*;
pub use order_dir::*;
use querystrong::QueryStrong;
use std::collections::BTreeMap;

#[derive(Debug)]
pub struct QueryParams {
    pub order_by: Option<String>,
    pub order_dir: Option<OrderDir>,
    pub offset: Option<u64>,
    pub limit: Option<u64>,
    pub r#where: Option<BTreeMap<String, String>>,
    pub start_with_and: bool,
}

impl QueryParams {
    pub fn start_with_and(mut self) -> Self {
        self.start_with_and = true;
        self
    }

    /// Validate the params - columns are the table columns
    pub fn validate(&mut self, columns: &[&str]) -> Result<(), sqlx::Error> {
        if let Some(w) = &mut self.r#where {
            for (k, v) in w {
                if !columns.contains(&k.as_str()) {
                    return Err(sqlx::Error::ColumnNotFound(k.to_owned()));
                }

                *v = Self::escape_special_chars(v.as_str())?;
            }

            if let Some(order_by) = &mut self.order_by {
                *order_by = Self::escape_special_chars(order_by.as_str())?;
            }
        }
        Ok(())
    }

    fn escape_special_chars(text: &str) -> Result<String, std::io::Error> {
        let mut res = String::new();
        for c in text.chars() {
            if ESCAPED_CHARS.contains(&c) {
                res.push('\\');
            }
            res.push(c);
        }
        Ok(res)
    }
}

impl std::fmt::Display for QueryParams {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut q = String::from("");

        if let Some(w) = &self.r#where {
            for (x, (key, value)) in w.iter().enumerate() {
                let condition = Condition::try_from(value.as_str()).map_err(|_| std::fmt::Error)?;

                if x == 0 && !self.start_with_and {
                    q.push_str(&format!(" WHERE {} {}", key, condition));
                } else {
                    q.push_str(&format!(" AND {} {}", key, condition));
                }
            }
        }

        match (&self.order_by, &self.order_dir) {
            (Some(x), Some(y)) => q.push_str(&format!(" ORDER BY {} {}", x, y)),
            (Some(x), _) => q.push_str(&format!(" ORDER BY {}", x)),
            (_, Some(y)) => q.push_str(&format!(" ORDER BY created {}", y)),
            _ => (),
        }

        if let Some(x) = &self.offset {
            q.push_str(&format!(" OFFSET {}", x))
        }
        if let Some(x) = &self.limit {
            q.push_str(&format!(" LIMIT {}", x))
        }

        write!(f, "{}", q)
    }
}

impl Default for QueryParams {
    fn default() -> Self {
        Self {
            order_by: None,
            order_dir: None,
            offset: None,
            limit: None,
            r#where: None,
            start_with_and: false,
        }
    }
}

impl From<QueryStrong> for QueryParams {
    fn from(q: QueryStrong) -> Self {
        let order_by = q.get_str("order_by").and_then(|x| Some(x.to_string()));
        let order_dir = q.get_str("order_dir").and_then(|x| x.try_into().ok());
        let offset = q.get_str("offset").and_then(|x| x.parse().ok());
        let limit = q.get_str("limit").and_then(|x| x.parse().ok());

        let r#where = q
            .get_map("where")
            .unwrap_or(&BTreeMap::new())
            .to_owned()
            .iter()
            .map(|(x, y)| {
                (
                    x.to_string(),
                    querystrong::Value::as_str(y)
                        .unwrap_or_default()
                        .to_string(),
                )
            })
            .collect::<BTreeMap<String, String>>();

        Self {
            order_by,
            order_dir,
            offset,
            limit,
            r#where: if r#where.len() == 0 {
                None
            } else {
                Some(r#where)
            },
            ..Default::default()
        }
    }
}

#[cfg(feature = "poem")]
#[poem::async_trait]
impl<'a> poem::web::FromRequest<'a> for QueryParams {
    async fn from_request(
        req: &'a poem::Request,
        _body: &mut poem::web::RequestBody,
    ) -> poem::Result<Self> {
        if let Some(q) = req.uri().query() {
            let query = QueryStrong::parse(q).map_err(|_| {
                poem::Error::from_string(
                    "Failed to parse query",
                    poem::http::StatusCode::BAD_REQUEST,
                )
            })?;
            Ok(query.into())
        } else {
            Ok(Self::default())
        }
    }
}

#[cfg(feature = "axum")]
#[axum::async_trait]
impl<S> axum::extract::FromRequestParts<S> for QueryParams
where
    S: Send + Sync,
{
    type Rejection = (axum::http::StatusCode, &'static str);

    async fn from_request_parts(
        parts: &mut axum::http::request::Parts,
        _: &S,
    ) -> Result<Self, Self::Rejection> {
        if let Some(query) = parts.uri.query() {
            let query = QueryStrong::parse(query)
                .map_err(|_| (axum::http::StatusCode::BAD_REQUEST, "Failed to parse query"))?;
            Ok(query.into())
        } else {
            Err((axum::http::StatusCode::BAD_REQUEST, "Failed to parse query"))
        }
    }
}

#[cfg(test)]
mod tests {
    use querystrong::QueryStrong;

    use super::*;

    const Q: &'static str = "where[name]=kalle&where[surname]=(ew)son&where[created]=(lt)2018-01-01&order_dir=asc&order_by=name&offset=1&limit=1";

    #[test]
    fn query_strong_accepts_empty() -> () {
        let res = QueryStrong::parse("");
        assert!(res.is_ok())
    }

    #[test]
    fn query_strong_to_options() -> Result<(), std::io::Error> {
        let query_string = QueryStrong::parse(Q).unwrap();
        let QueryParams {
            order_by,
            order_dir,
            offset,
            limit,
            r#where,
            start_with_and,
        } = QueryParams::from(query_string);

        let w = r#where.unwrap();

        assert_eq!(order_dir, Some(OrderDir::Asc));
        assert_eq!(order_by, Some("name".into()));
        assert_eq!(offset, Some(1));
        assert_eq!(limit, Some(1));
        assert_eq!(w["name"], "kalle");
        assert_eq!(w["created"], "(lt)2018-01-01");
        assert_eq!(start_with_and, false);
        Ok(())
    }

    #[test]
    fn options_to_sql() -> Result<(), std::io::Error> {
        let query_string = QueryStrong::parse(Q).unwrap();
        let params = QueryParams::from(query_string);
        assert_eq!(format!("{}", params), " WHERE created < '2018-01-01' AND name = 'kalle' AND surname '%son' ORDER BY name asc OFFSET 1 LIMIT 1");
        Ok(())
    }
    #[test]
    fn options_to_sql_start_with_and() -> Result<(), std::io::Error> {
        let query_string = QueryStrong::parse(Q).unwrap();
        let params = QueryParams::from(query_string).start_with_and();
        assert_eq!(format!("{}", params), " AND created < '2018-01-01' AND name = 'kalle' AND surname '%son' ORDER BY name asc OFFSET 1 LIMIT 1");
        Ok(())
    }

    #[test]
    fn options_validate_escapes_bad_chars() -> Result<(), std::io::Error> {
        let columns = ["name", "posts", "comments"];
        let query_string = QueryStrong::parse("where[name]=(eq)kalle%");
        let mut params = QueryParams::from(query_string.unwrap());
        params.validate(&columns).unwrap();

        let binding = params.r#where.unwrap();
        let value = binding.get("name").unwrap();
        assert_eq!(value, "(eq)kalle\\%");
        Ok(())
    }
}
