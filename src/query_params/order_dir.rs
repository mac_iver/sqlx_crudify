use std::fmt::Display;

#[derive(Debug, PartialEq, Eq)]
pub enum OrderDir {
    Asc,
    Desc,
}

impl TryFrom<&str> for OrderDir {
    type Error = std::io::Error;
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "asc" => Ok(Self::Asc),
            "desc" => Ok(Self::Desc),
            _ => Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "invalid order_dir",
            )),
        }
    }
}

impl Display for OrderDir {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Asc => write!(f, "asc"),
            Self::Desc => write!(f, "desc"),
        }
    }
}
