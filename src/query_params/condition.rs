#[derive(Debug)]
pub enum Condition {
    Eq(String),
    GreaterThan(String),
    LessThan(String),
    GreaterThanOrEqual(String),
    LessThanOrEqual(String),
    NotEq(String),
    StartsWith(String),
    EndsWith(String),
}

impl std::fmt::Display for Condition {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Eq(ref x) => write!(f, "= '{}'", x),
            Self::GreaterThan(ref x) => write!(f, "> '{}'", x),
            Self::LessThan(ref x) => write!(f, "< '{}'", x),
            Self::GreaterThanOrEqual(ref x) => write!(f, ">= '{}'", x),
            Self::LessThanOrEqual(ref x) => write!(f, "<= '{}'", x),
            Self::NotEq(ref x) => write!(f, "!= '{}'", x),
            Self::StartsWith(ref x) => write!(f, "'{}%'", x),
            Self::EndsWith(ref x) => write!(f, "'%{}'", x),
        }
    }
}

impl TryFrom<&str> for Condition {
    type Error = std::io::Error;
    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let res = match s {
            s if s.starts_with("(eq)") => Self::Eq(s.replacen("(eq)", "", 1)),
            s if s.starts_with("(gt)") => Self::GreaterThan(s.replacen("(gt)", "", 1)),
            s if s.starts_with("(lt)") => Self::LessThan(s.replacen("(lt)", "", 1)),
            s if s.starts_with("(go)") => Self::GreaterThanOrEqual(s.replacen("(go)", "", 1)),
            s if s.starts_with("(lo)") => Self::LessThanOrEqual(s.replacen("(go)", "", 1)),
            s if s.starts_with("(no)") => Self::NotEq(s.replacen("(no)", "", 1)),
            s if s.starts_with("(sw)") => Self::StartsWith(s.replacen("(sw)", "", 1)),
            s if s.starts_with("(ew)") => Self::EndsWith(s.replacen("(ew)", "", 1)),
            s => Self::Eq(s.to_owned()),
        };

        Ok(res)
    }
}
