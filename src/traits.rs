use async_trait::async_trait;
use sqlx::{
    database::HasArguments, Database, Executor, FromRow, IntoArguments, QueryBuilder, Result, Row,
};
use std::fmt::Debug;
use uuid::Uuid;

use crate::QueryParams;

#[async_trait]
pub trait Crud<C, R, U, D, Db>
where
    Self: Debug + Sized + Send + Unpin + for<'r> FromRow<'r, Db::Row> + Crudify,
    C: Debug + Send + Crudify,
    R: Debug + Send + Crudify,
    U: Debug + Send + Crudify,
    D: Debug + Send + Crudify,
    Db: Database,
{
    async fn db_create<'e, E>(executor: E, data: Vec<C>) -> Result<Vec<Self>>
    where
        E: Executor<'e, Database = Db> + Send + Sync + 'e,
        <Db as HasArguments<'e>>::Arguments: IntoArguments<'e, Db>,
    {
        let mut query = C::create_query(data)?;
        let res = query.build_query_as::<Self>().fetch_all(executor).await?;
        Ok(res)
    }

    async fn db_read<'e, E>(
        executor: E,
        data: Option<Vec<Uuid>>,
        mut params: QueryParams,
    ) -> Result<Vec<Self>>
    where
        E: Executor<'e, Database = Db> + Send + Sync + 'e,
        <Db as HasArguments<'e>>::Arguments: IntoArguments<'e, Db>,
    {
        params.validate(Self::FIELDS)?;
        let mut query = R::read_query(data, params)?;
        let res = query.build_query_as::<Self>().fetch_all(executor).await?;
        Ok(res)
    }

    async fn db_update<'e, E>(executor: E, data: Vec<U>) -> Result<Vec<Self>>
    where
        E: Executor<'e, Database = Db> + Send + Sync + 'e,
        <Db as HasArguments<'e>>::Arguments: IntoArguments<'e, Db>,
    {
        let mut query = U::update_query(data)?;
        let res = query.build_query_as::<Self>().fetch_all(executor).await?;
        Ok(res)
    }

    async fn db_delete<'e, E>(
        executor: E,
        ids: Option<Vec<Uuid>>,
        mut params: QueryParams,
    ) -> Result<Vec<Uuid>>
    where
        E: Executor<'e, Database = Db> + Send + Sync + 'e,
        <Db as HasArguments<'e>>::Arguments: IntoArguments<'e, Db>,
    {
        params.validate(Self::FIELDS)?;
        let mut query = D::delete_query(ids, params)?;
        let res = query
            .build()
            .map(|row: <Db as sqlx::Database>::Row| row.get(D::ID))
            .fetch_all(executor)
            .await?;
        Ok(res)
    }
}

pub trait DbConst {
    const TABLE: &'static str;
    const FIELDS: &'static [&'static str];
}

pub trait Crudify
where
    Self: Sized,
{
    const ID: &'static str;
    /// The table name used when creating db queries
    const TABLE: &'static str;
    /// All of the struct fields (database columns)
    const FIELDS: &'static [&'static str];

    // TODO: Add read params

    /// The structures type schema. Some = 1, None = 0, Required value = 2
    /// This is used to compare each struct in an array so that the number of generated query names & values
    /// are equal.
    fn query_schema(&self) -> Vec<u8>;

    /// Generate an insert query
    #[allow(unused_variables)]
    fn create_query<'a, Db>(data: Vec<Self>) -> Result<QueryBuilder<'a, Db>>
    where
        Db: Database,
    {
        unimplemented!()
    }

    /// Generate a select query
    #[allow(unused_variables)]
    fn read_query<'a, Db>(
        ids: Option<Vec<Uuid>>,
        params: QueryParams,
    ) -> Result<QueryBuilder<'a, Db>>
    where
        Db: Database,
    {
        unimplemented!()
    }

    /// Generate a insert and update query
    #[allow(unused_variables)]
    fn update_query<'a, Db>(data: Vec<Self>) -> Result<QueryBuilder<'a, Db>>
    where
        Db: Database,
    {
        unimplemented!()
    }

    /// Generate a delete query
    #[allow(unused_variables)]
    fn delete_query<'a, Db>(
        ids: Option<Vec<Uuid>>,
        params: QueryParams,
    ) -> Result<QueryBuilder<'a, Db>>
    where
        Db: Database,
    {
        unimplemented!()
    }
}
