mod item;

use axum::{
    response::IntoResponse,
    routing::{get, post},
    Router,
};
use sqlx::sqlite::SqlitePoolOptions;

async fn index_handler() -> impl IntoResponse {
    "Hello! Visit /item to get started."
}

async fn create_item() -> impl IntoResponse {
    "create"
}

async fn read_item() -> impl IntoResponse {
    "read"
}

async fn update_item() -> impl IntoResponse {
    "update"
}

async fn delete_item() -> impl IntoResponse {
    "delete"
}

#[tokio::main]
async fn main() {
    let pool = SqlitePoolOptions::new()
        .connect("sqlite://item.db")
        .await
        .unwrap();

    // build our application with a single route
    let app = Router::new()
        .route("/", get(index_handler))
        .route(
            "/item",
            post(create_item)
                .get(read_item)
                .put(update_item)
                .delete(delete_item),
        )
        .with_state(pool);

    let address = "[::]:3000";
    println!("Running server on: {}", &address);

    axum::Server::bind(&address.parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}
