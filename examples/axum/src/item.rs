use serde::{Deserialize, Serialize};
use sqlx::FromRow;
use sqlx_crudify::*;

#[derive(Debug, Clone, FromRow, Serialize, Deserialize, Crudify)]
#[crudify(table = "item")]
pub struct Item {
    id: i64,
    name: String,
    surname: Option<String>,
}

#[derive(Debug, Clone, FromRow, Serialize, Deserialize, Crudify)]
pub struct ItemCreate {
    pub name: String,
    pub surname: Option<String>,
}

#[derive(Debug, Crudify)]
#[crudify(table = "item", action = "read")]
pub struct ItemRead;

#[derive(Debug, Clone, FromRow, Serialize, Deserialize, Crudify)]
pub struct ItemUpdate {
    pub name: String,
    pub surname: Option<String>,
}

#[derive(Debug, Clone, FromRow, Serialize, Deserialize, Crudify)]
pub struct ItemDelete {
    pub id: i64,
}

impl Crud<ItemCreate, ItemRead, ItemUpdate, ItemDelete> for Item {}
