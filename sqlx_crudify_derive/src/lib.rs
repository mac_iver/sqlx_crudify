#![allow(dead_code)]

use darling::FromDeriveInput;
use proc_macro2::{self, Span, TokenStream};
use quote::quote;
use syn::{parse_macro_input, Data, DeriveInput, Fields, Ident};

// TODO: Use db_type & config?
// mod config;
// mod db_type;
mod struct_field;

use struct_field::StructField;

#[derive(FromDeriveInput, Default)]
#[darling(default, attributes(crudify))]
struct StructAttr {
    table: String,
    action: Option<String>,
    id: Option<String>,
}

// TODO: Improve error handling

#[proc_macro_derive(Crudify, attributes(crudify))]
pub fn derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input);
    let StructAttr {
        action, table, id, ..
    } = StructAttr::from_derive_input(&input).expect("Wrong params");
    let DeriveInput { ident, data, .. } = input;

    let id = if let Some(id) = id {
        Ident::new(&id, Span::call_site())
    } else {
        Ident::new("id", Span::call_site())
    };

    let attrs = attributes(&data);
    let attr_names = get_attr_names(&attrs);
    let query_schema = query_schema(&attrs);

    let func = match action {
        Some(x) if x == "create" => create_query(&attrs),
        Some(x) if x == "read" => read_query(&table),
        Some(x) if x == "update" => update_query(&attrs, &id),
        Some(x) if x == "delete" => delete_query(&table, &id),
        _ => quote! {},
    };

    let output = quote! {
        impl Crudify for #ident {
            const ID: &'static str = stringify!(#id);
            const TABLE: &'static str = #table;
            const FIELDS: &'static [&'static str] = &[#attr_names];

            #query_schema
            #func
        }
    };
    proc_macro::TokenStream::from(output)
}

fn get_attr_names(attrs: &[StructField]) -> TokenStream {
    let mut all_fields = attrs.iter().map(|x| x.key_string()).collect::<Vec<_>>();
    all_fields.sort();
    quote! { #(#all_fields),* }
}

// Some = 1, None = 0, Required (non-option value) = 2
fn query_schema(attrs: &Vec<StructField>) -> TokenStream {
    let mut required_map = vec![];
    let mut optional_map = vec![];

    for x in attrs {
        if x.is_option() {
            optional_map.push(&x.key);
        } else {
            required_map.push(2_u8);
        }
    }

    quote! {
        fn query_schema(&self) -> Vec<u8> {
            let mut vec = vec![];

            #(vec.push(#required_map);)*
            #(
                if self.#optional_map.is_some() {
                    vec.push(1_u8);
                } else {
                    vec.push(0_u8);
                }
            )*

            vec
        }
    }
}

fn attributes(data: &Data) -> Vec<StructField> {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => fields
                .named
                .iter()
                .map(|x| x.clone().into())
                .collect::<Vec<_>>(),
            _ => vec![],
        },
        _ => panic!("Only structs are supported"),
    }
}

fn create_query(attrs: &Vec<StructField>) -> TokenStream {
    let mut required_attrs = vec![];
    let mut optional_attrs = vec![];

    for x in attrs {
        if x.is_option() {
            optional_attrs.push(&x.key);
        } else {
            required_attrs.push(&x.key);
        }
    }

    quote! {
        fn create_query<'a>(data: Vec<Self>) -> std::result::Result<QueryBuilder<'a, Postgres>, sqlx::Error> {
            let first = if let Some(item) = data.first() {
                item
            } else {
                return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Missing item").into())
            };

            let type_schema = first.query_schema();

            for x in data.iter().skip(1) {
                if x.query_schema() != type_schema {
                    return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Items are not uniform").into())
                }
            }

            let mut start = vec![];

            #(start.push(stringify!(#required_attrs));)*
            #(
                if first.#optional_attrs.is_some() {
                    start.push(stringify!(#optional_attrs));
                }
            )*

            let mut query = QueryBuilder::new(format!("INSERT INTO {} ({}) ", Self::TABLE, start.join(", ")));

            query.push_values(data.iter().take(1024), |mut req, data| {
                #(req.push_bind(data.#required_attrs.clone());)*
                #(
                    if data.#optional_attrs.is_some() {
                        req.push_bind(data.#optional_attrs.clone());
                    }
                )*
            });
            query.push(" RETURNING *");

            Ok(query)
        }
    }
}

fn read_query(table: &str) -> TokenStream {
    quote! {
        fn read_query<'a>(ids: Option<Vec<Uuid>>, params: QueryParams) -> std::result::Result<QueryBuilder<'a, Postgres>, sqlx::Error> {
            let ids = ids.unwrap_or_default();
            let mut query = QueryBuilder::new(format!("SELECT * FROM {}", &#table));
            if !ids.is_empty() {
                query.push(" WHERE id = ANY(").push_bind(ids).push(")");
            }

            query.push(format!("{}", params));

            Ok(query)
        }
    }
}

fn update_query(attrs: &Vec<StructField>, id: &Ident) -> TokenStream {
    // NOTE: Using the #var inside quote! requires unique vectors when looping
    let mut id_attr = String::from("");
    let mut required_fields = vec![];
    let mut required_fields_attr = vec![];
    let mut optional_fields = vec![];
    let mut optional_fields_attr = vec![];

    for x in attrs {
        if *id == x.key_string() {
            id_attr.push_str(&x.attr())
        }
        if x.is_option() {
            optional_fields.push(&x.key);
            optional_fields_attr.push(x.attr());
        } else if *id != x.key_string() {
            required_fields.push(&x.key);
            required_fields_attr.push(x.attr());
        }
    }

    quote! {
        fn update_query<'a>(data: Vec<Self>) -> std::result::Result<QueryBuilder<'a, Postgres>, sqlx::Error> {
            let first = if let Some(item) = data.first() {
                item
            } else {
                return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Missing item").into())
            };

            let type_schema = first.query_schema();

            for x in data.iter().skip(1) {
                if x.query_schema() != type_schema {
                    return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Items are not uniform").into())
                }
            }

            let mut start: Vec<String> = vec![];
            #(start.push(format!("{x} = x2.{x}{y}", x = stringify!(#required_fields), y = #required_fields_attr));)*
            #(
                if first.#optional_fields.is_some() {
                    start.push(format!("{x} = x2.{x}{y}", x = stringify!(#optional_fields), y = #optional_fields_attr));
                }
            )*

            let mut query = QueryBuilder::new(format!("UPDATE {} AS x SET {} FROM (", Self::TABLE, start.join(", ")));

            query.push_values(data.iter().take(1024), |mut req, data| {
                req.push_bind(data.#id.clone());
                #(req.push_bind(data.#required_fields.clone());)*
                #(
                    if data.#optional_fields.is_some() {
                        req.push_bind(data.#optional_fields.clone());
                    }
                )*
            });

            let mut end: Vec<&str> = vec![stringify!(#id)];

            #(end.push(stringify!(#required_fields));)*
            #(
                if first.#optional_fields.is_some() {
                    end.push(stringify!(#optional_fields));
                }
            )*

            query.push(format!(
                ") AS x2 ({x}) WHERE x2.{y}{z} = x.{y} RETURNING *;",
                x = end.join(", "),
                y = stringify!(#id),
                z = #id_attr
            ));

            Ok(query)
        }
    }
}

fn delete_query(table: &str, id: &Ident) -> TokenStream {
    quote! {
        fn delete_query<'a>(ids: Option<Vec<Uuid>>, params: QueryParams) -> std::result::Result<QueryBuilder<'a, Postgres>, sqlx::Error> {
            let ids = ids.unwrap_or_default();
            let mut query = QueryBuilder::new(format!("DELETE FROM {} *", &#table));
            if !ids.is_empty() {
                query.push(format!(" WHERE {} = ANY(", stringify!(#id))).push_bind(ids).push(")");
            }
            query.push(format!("{}", params));
            query.push(format!(" RETURNING {}", stringify!(#id)));
            Ok(query)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proc_macro2::TokenStream;
    use std::str::FromStr;
    use syn::ItemStruct;

    #[test]
    fn this_is_how_you_debug() {
        let s = "
        pub struct Create {
            pub name: String,
            pub age: Option<u64>,
            #[crudify(uuid)] pub uuid: Option<Uuid>,
        }";
        let tokens = TokenStream::from_str(s).unwrap();
        let ast: ItemStruct = syn::parse2(tokens).unwrap();
        println!("{:#?}", ast);
    }
}
