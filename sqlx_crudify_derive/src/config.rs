use crate::db_type::DbType;
use inflector::Inflector;
use proc_macro2::{Ident, TokenStream};
use quote::quote;
use syn::{punctuated::Punctuated, token::Comma, Attribute, Field};

struct Config {
    ident: Ident,
    crate_name: TokenStream,
    db_type: DbType,
    table_name: String,
    id_column: Ident,
}

impl Config {
    fn new(ident: &Ident, attrs: &[Attribute], fields: &Punctuated<Field, Comma>) -> Self {
        let crate_name = std::env::var("CARGO_PKG_NAME").unwrap();
        let db_type = DbType::new(attrs);

        // Use attr table otherwise default to struct name (table_case)
        let table_name = fields
            .iter()
            .find(|f| f.attrs.iter().any(|a| a.path.is_ident("table")))
            .and_then(|f| f.ident.as_ref())
            .and_then(|f| Some(f.to_string()))
            .unwrap_or_else(|| ident.to_string().to_table_case());

        // The id
        let id = fields
            .iter()
            .find(|f| f.attrs.iter().any(|a| a.path.is_ident("id")))
            .and_then(|f| f.ident.as_ref())
            .unwrap_or_else(|| {
                fields
                    .iter()
                    .flat_map(|f| &f.ident)
                    .next()
                    .expect("first field")
            })
            .clone();

        Self {
            ident: ident.clone(),
            crate_name: quote! { crate_name },
            db_type,
            table_name,
        }
    }
}
