use proc_macro2::Ident;
use syn::{Attribute, Field, GenericArgument, PathSegment, Type};

/// Each struct field
#[derive(Debug)]
pub struct StructField {
    pub key: Ident,
    pub value: Vec<Ident>,
    pub attr: Vec<String>,
}

impl StructField {
    /// Check if field value is of the type Option
    pub fn is_option(&self) -> bool {
        if let Some(x) = self.value.first() {
            x == "Option"
        } else {
            false
        }
    }

    /// The field ident as string
    pub fn key_string(&self) -> String {
        self.key.to_string()
    }

    /// Attributes passed before each struct field
    pub fn attr(&self) -> String {
        if let Some(attr) = self.attr.get(0) {
            attr.to_owned()
        } else {
            String::from("")
        }
    }

    fn get_attrs(attrs: Vec<Attribute>) -> Vec<String> {
        if attrs.is_empty() {
            return vec![];
        }

        attrs
            .iter()
            .filter_map(|a| {
                a.path.segments.first().and_then(|x| {
                    if x.ident == "crudify" {
                        Some(format!(
                            "::{}",
                            a.tokens.to_string().replace(['(', ')'], "")
                        ))
                    } else {
                        None
                    }
                })
            })
            .collect::<Vec<_>>()
    }

    fn get_value(segment: &PathSegment) -> Vec<Ident> {
        let mut arr = vec![segment.ident.clone()];
        let arguments = match &segment.arguments {
            syn::PathArguments::AngleBracketed(angel_bracket) => angel_bracket
                .args
                .iter()
                .flat_map(|y| match y {
                    GenericArgument::Type(Type::Path(tp)) => {
                        let z = tp.path.segments.iter().fold(Vec::new(), |mut acc, x| {
                            acc.push(x.ident.clone());
                            acc
                        });
                        z
                    }
                    _ => panic!("only type path are supported"),
                })
                .collect::<Vec<_>>(),
            syn::PathArguments::None => vec![],
            _ => panic!("only angel brackets are supported"),
        };

        arr.extend(arguments);
        arr
    }
}

impl From<Field> for StructField {
    fn from(t: Field) -> Self {
        let Field {
            ident, ty, attrs, ..
        } = t;
        let res = match (ident, ty) {
            (Some(key), Type::Path(tp)) => {
                let value = Self::get_value(tp.path.segments.first().unwrap());
                let attr = Self::get_attrs(attrs);
                Self { key, value, attr }
            }
            _ => panic!("Only TypePath are supported"),
        };
        res
    }
}
