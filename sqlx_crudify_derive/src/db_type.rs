use proc_macro2::TokenStream;
use quote::quote;
use syn::{Attribute, Lit, Meta, MetaNameValue};

pub enum DbType {
    Any,
    Mssql,
    MySql,
    Postgres,
    Sqlite,
}

impl From<&str> for DbType {
    fn from(db_type: &str) -> Self {
        match db_type {
            "Any" => Self::Any,
            "Mssql" => Self::Mssql,
            "MySql" => Self::MySql,
            "Postgres" => Self::Postgres,
            "Sqlite" => Self::Sqlite,
            _ => panic!("unknown #[database] type {}", db_type),
        }
    }
}

impl DbType {
    pub fn new(attrs: &[Attribute]) -> Self {
        match attrs
            .iter()
            .find(|a| a.path.is_ident("database"))
            .map(|a| a.parse_meta())
        {
            Some(Ok(Meta::NameValue(MetaNameValue {
                lit: Lit::Str(s), ..
            }))) => DbType::from(&*s.value()),
            _ => Self::Sqlite,
        }
    }

    pub fn sqlx_db(&self) -> TokenStream {
        match self {
            Self::Any => quote! { ::sqlx::Any },
            Self::Mssql => quote! { ::sqlx::Mssql },
            Self::MySql => quote! { ::sqlx::MySql },
            Self::Postgres => quote! { ::sqlx::Postgres },
            Self::Sqlite => quote! { ::sqlx::Sqlite },
        }
    }

    pub fn quote_ident(&self, ident: &str) -> String {
        match self {
            Self::Any => format!(r#""{}""#, &ident),
            Self::Mssql => format!(r#""{}""#, &ident),
            Self::MySql => format!("`{}`", &ident),
            Self::Postgres => format!(r#""{}""#, &ident),
            Self::Sqlite => format!(r#""{}""#, &ident),
        }
    }
}
